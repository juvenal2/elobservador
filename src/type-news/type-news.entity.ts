import { News } from 'src/news/news.entity';
import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';

@Entity({name:'type_news'})
export class TypeNews{
    @PrimaryGeneratedColumn()
    id_type: number;
    
    @Column()
	name_type: string;

    //@Column({default: true})
    @Column()
	is_active: boolean;	

    @OneToMany(() => News, (news) => news.typeNews)
    news: News[];
}