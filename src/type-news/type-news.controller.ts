import { Controller, Post, Get, Put, Patch, Delete, Body, Param, ParseIntPipe } from '@nestjs/common';
import { TypeNewsService } from './type-news.service';
import { CreateTypeNewsDto } from './dto/create-type-news.dto';
import { TypeNews } from './type-news.entity';


@Controller('type')
export class TypeNewsController {
    constructor(private typeNewsService: TypeNewsService){}

    @Get()
    getListTypeNews(): Promise<TypeNews[]>{
        return this.typeNewsService.getLisTypeNews();
    }

    @Get(':id')
    getTypeNews(@Param('id', ParseIntPipe)id: number){
        return this.typeNewsService.getTypeNews(id);
    }

    @Post()
    createTypeNews(@Body() typeNewsDto: CreateTypeNewsDto){
        return this.typeNewsService.createTypeNews(typeNewsDto);
    }

    @Patch(':id')
    updateTypeNews(@Param('id', ParseIntPipe)id: number, @Body() typeNewDto: CreateTypeNewsDto){
        return this.typeNewsService.updateTypeNews(id, typeNewDto);
    }

    @Delete(':id')
    deleteTypeNews(@Param('id', ParseIntPipe) id: number){
        return this.typeNewsService.deleteTypeNews(id);
    }
}
