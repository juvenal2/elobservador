import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeNews } from './type-news.entity';
import { CreateTypeNewsDto } from './dto/create-type-news.dto';

@Injectable()
export class TypeNewsService {
    constructor(
        @InjectRepository(TypeNews) private typeNewsRepository:Repository<TypeNews>){}
    
    getLisTypeNews(){
        return this.typeNewsRepository.find();
    }

    async getTypeNews(id: number){
        const typeNewsFound = await this.typeNewsRepository.findOne({
            where: {
                id_type: id
            },
            //relations: ['portal']
        });

        if(!typeNewsFound){
            return new HttpException('Tipo de noticia no encotrado.', HttpStatus.NOT_FOUND);
        }
        return typeNewsFound;
    }

    async createTypeNews(typeNewsDto: CreateTypeNewsDto){
        const typeNewsFound = await this.typeNewsRepository.findOne({
            where:{
                name_type: typeNewsDto.name_type
            }
        });
        console.log(typeNewsDto);

        if(typeNewsFound){
            return new HttpException('Clasificacion de noticia existe.', HttpStatus.CONFLICT);
        }

        typeNewsDto.is_active= true;
        const newTypeNews = this.typeNewsRepository.create(typeNewsDto);
        return this.typeNewsRepository.save(newTypeNews);
    }

    async updateTypeNews(id: number, typeNewDto: CreateTypeNewsDto){
        const typeNewsFound = await this.typeNewsRepository.findOne({
            where:{
                id_type: id
            }     
        });

        if(!typeNewsFound)
            return new HttpException('Tipo de noticia no encontrado', HttpStatus.NOT_FOUND);

        const updateTypeNews = Object.assign(typeNewsFound, typeNewDto);
        return this.typeNewsRepository.save(updateTypeNews);
    }

    async deleteTypeNews(id: number){
        const result = await this.typeNewsRepository.findOne({
            where: {
                id_type: id
            },
        });

        if(!result){
            return new HttpException('Typo de noticia no encontrado', HttpStatus.NOT_FOUND)
        }

        if(result.is_active == true){
            result.is_active = false;
        }

        return this.typeNewsRepository.save(result);
    }
}
