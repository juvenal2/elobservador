import { Module } from '@nestjs/common';
import { TypeNewsController } from './type-news.controller';
import { TypeNewsService } from './type-news.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeNews } from './type-news.entity';
import { News } from 'src/news/news.entity';

@Module({
  imports:[TypeOrmModule.forFeature([TypeNews, News]), ],
  controllers: [TypeNewsController],
  providers: [TypeNewsService],
  exports: [TypeNewsService]
})
export class TypeNewsModule {}
