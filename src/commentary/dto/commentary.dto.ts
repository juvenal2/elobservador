export class CommentaryDto{
	id_news: number;
	name_users: string;
	commentaries: string;
	qualification: number;
	dates: Date;
}