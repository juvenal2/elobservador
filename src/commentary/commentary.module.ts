import { Module } from '@nestjs/common';
import { CommentaryController } from './commentary.controller';
import { CommentaryService } from './commentary.service';
import { Commentary } from './commentary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewsModule } from 'src/news/news.module';

@Module({
  imports:[TypeOrmModule.forFeature([Commentary]), NewsModule],
  controllers: [CommentaryController],
  providers: [CommentaryService],
  exports: [CommentaryService]
})
export class CommentaryModule {}
