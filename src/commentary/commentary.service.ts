import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Commentary } from './commentary.entity';
import { CommentaryDto } from './dto/commentary.dto';

@Injectable()
export class CommentaryService {
    constructor(
        @InjectRepository(Commentary) private commentaryRepository:Repository<Commentary>){}

        createCommentary(commentaryDto: CommentaryDto){
            
            const newCommentary = this.commentaryRepository.create(commentaryDto)
            return this.commentaryRepository.save(newCommentary)
        }

        getListCommentary(){
            return this.commentaryRepository.find();
        }
}
