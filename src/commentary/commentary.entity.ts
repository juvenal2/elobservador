import { News } from 'src/news/news.entity';
import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';

@Entity({name:'commentary'})
export class Commentary{
	@PrimaryGeneratedColumn()
    id_comment: number;
	@Column()
	id_news: number;
	@Column()
	name_users: string;
	@Column()
	commentaries: string;
	@Column()
	qualification: number;
	@Column({ type: 'date', default: () => 'CURRENT_DATE' })
	dates: Date;

	@ManyToOne(() => News, (news) => news.commentary)
    @JoinColumn({ name: 'id_news'})
    news:News;
}