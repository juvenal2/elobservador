import { Controller, Post, Get, Put, Patch, Delete, Body, Param, ParseIntPipe } from '@nestjs/common';
import { CommentaryService } from './commentary.service';
import { Commentary } from './commentary.entity';
import { CommentaryDto } from './dto/commentary.dto';

@Controller('commentary')
export class CommentaryController {
    constructor( private commentaryService: CommentaryService){}
    //Lista de comentarios
    @Get()
    getListUsers(): Promise<Commentary[]>{
        return this.commentaryService.getListCommentary();
    }

    //agrega comentarios a la tabla Commentary de base de datos 'TheObserverDB'
    @Post()
    createUser(@Body() newCommentary: CommentaryDto){
        return this.commentaryService.createCommentary(newCommentary);
    }
}
