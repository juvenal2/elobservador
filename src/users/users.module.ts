import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import { User } from './user.entity';
import { News } from 'src/news/news.entity';

@Module({
  //imports:[TypeOrmModule.forFeature([User, News])],
  imports:[TypeOrmModule.forFeature([User, News])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
