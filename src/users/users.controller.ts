import { Controller, Post, Get, Put, Patch, Delete, Body, Param, ParseIntPipe } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto'
import { UsersService } from './users.service';
import { User } from './user.entity';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {
    constructor( private usersService: UsersService){}
    //Lista de usuarios
    @Get()
    getListUsers(): Promise<User[]>{
        return this.usersService.getListUsers();
    }

    //muestra los datos segun el id del usuario
    @Get(':id')
    getUser(@Param('id', ParseIntPipe) id: number){
        //console.log(id);
        //console.log(typeof id);
        return this.usersService.getUser(id);
    }

    //agrega usuarios a la base de datos 'TheObserverDB'
    @Post()
    createUser(@Body() newUser: CreateUserDto){
        return this.usersService.createUser(newUser);
    }

    //elimina el registro segun el id_usuario
    @Delete(':id')
    deleteUser(@Param('id', ParseIntPipe) id: number){
       return this.usersService.deleteUser(id);
    }

    @Patch(':id')
    updateUser(@Param('id', ParseIntPipe) id: number, @Body() user: UpdateUserDto){
        return this.usersService.updateUser(id, user);
    }
}
