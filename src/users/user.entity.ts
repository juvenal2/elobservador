import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import {News} from 'src/news/news.entity'

@Entity({name:'users'})
export class User{
    @PrimaryGeneratedColumn()
    id_user: number;

    @Column({unique: true})
    ci: number;

    @Column()
	name_user: string;

    @Column({unique: true})
	username: String;

    @Column()
	passwords: string;

    @Column()
	is_active: boolean;

    @Column()
	roles: string;

    @OneToMany(() => News, (news) => news.user)
    news: News[];
}

enum Roles{
    REDACTOR = 'REDACTOR',
    EDITOR = 'EDITOR',
    SUSCRIPTOR = 'SUSCRIPTOR'
}