import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import {CreateUserDto} from './dto/create-user.dto';
import {UpdateUserDto} from './dto/update-user.dto';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private userRepository:Repository<User>){}

        async createUser(user: CreateUserDto){
            const userFoundCI = await this.userRepository.findOne({
                where:{
                    ci: user.ci
                },
                
            });

            const userFoundN = await this.userRepository.findOne({
                where:{
                    username: user.username                
                },
                
            });

            if(userFoundCI){
                return new HttpException('Cedula de Identidad '+ user.ci + ' existe.', HttpStatus.CONFLICT);
            }
            if(userFoundN){
                return new HttpException('Usuario '+ user.username + ' existe', HttpStatus.CONFLICT);
            }
            user.is_active = true;
            const newUser = this.userRepository.create(user);
            return this.userRepository.save(newUser);
        }

        getListUsers(){
            return this.userRepository.find();
        }

        async getUser(id: number){
            
            const userFound = await this.userRepository.findOne({
                where: {
                    id_user: id
                },
                relations: ['news']
            });

            if(!userFound){
                return new HttpException('Usuario no encontrado', HttpStatus.NOT_FOUND);
            }
            return userFound;
        }

        async deleteUser(id: number){
            const result = await this.userRepository.delete(id);

            if(result.affected === 0){
                return new HttpException('Usuario no encontrado', HttpStatus.NOT_FOUND)
            }
            return result;
        }

        async updateUser(id: number, user: UpdateUserDto ){
            const userFound = await this.userRepository.findOne({
                where:{
                    id_user: id
                }     
            });

            if(!userFound){
                return new HttpException('Usuario no encontrado', HttpStatus.NOT_FOUND);

            }

            const updateUser = Object.assign(userFound, user);
            return this.userRepository.save(updateUser);
        }
}
