export class CreateUserDto{
	ci: number;
	name_user: string;
	username: String;
	passwords: string;
	is_active: boolean;
	roles: string;
}