import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Portal } from './portal.entity';
import { CreatePortalDto } from './dto/create-portal.dto';

@Injectable()
export class PortalService {
    constructor(@InjectRepository(Portal) private portalRepository: Repository<Portal>){}

    getListPortal(){
        return this.portalRepository.find();
    }

    async getPortal(id: number){
        
        const portalFound = await this.portalRepository.findOne({
            where: {
                id_portal: id
            },
            relations: ['typeNews']
        });

        if(!portalFound){
            return new HttpException('Portal no encontrado', HttpStatus.NOT_FOUND);
        }
        return portalFound;
    }

    async createPortal(portalDto: CreatePortalDto){
        portalDto.approves = false;
        portalDto.is_active = true;
        portalDto.dates = new Date();
        
        const newPortal = this.portalRepository.create(portalDto);
        return this.portalRepository.save(newPortal);
    }

    
    async updatePortal(id: number, portalDto: CreatePortalDto ){
        const portalFound = await this.portalRepository.findOne({
            where:{
                id_user: id
            }     
        });
        
        if(!portalFound){
            return new HttpException('Portal no encontrado', HttpStatus.NOT_FOUND);
            
        }
        
        const updatePortal = Object.assign(portalFound, portalDto);
        return this.portalRepository.save(updatePortal);
    }

    async deletePortal(id: number){
        const portalFound = await this.portalRepository.findOne({
            where:{
                id_portal: id
            }
        });
    
        if(!portalFound){
            return new HttpException('Portal no encontrado', HttpStatus.NOT_FOUND)
        }

        if(portalFound.is_active == true){
            portalFound.is_active = false;
        }
        return this.portalRepository.save(portalFound);
    }

    async approvedPortal(id: number){
        const result = await this.portalRepository.findOne({
            where:{
                id_portal: id
            }
        });

        if(result.approves == false){
            result.is_active = true;
        }
        return this.portalRepository.save(result);
    }
}
