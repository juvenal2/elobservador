export class CreatePortalDto{
	id_user: number;
	id_type: number;
	id_news: number;
	contents: string;
	approves: boolean;	
	is_active: boolean;
	dates: Date;
}