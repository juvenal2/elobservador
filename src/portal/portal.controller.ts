import { Controller, Post, Get, Put, Patch, Delete, Body, Param, ParseIntPipe } from '@nestjs/common';
import { PortalService } from './portal.service';
import { Portal } from './portal.entity';
import { CreatePortalDto } from './dto/create-portal.dto';

@Controller('portal')
export class PortalController {
    constructor( private portalService: PortalService){}

    @Get()
    getListPortal(): Promise<Portal[]>{
        return this.portalService.getListPortal();
    }

    @Get(':id')
    getPortal(@Param('id', ParseIntPipe) id: number){
        return this.portalService.getPortal(id);
    }

    @Post()
    createPortal(@Body() newPortal: CreatePortalDto){
        return this.portalService.createPortal(newPortal);
    }

    @Patch(':id')
    updateUser(@Param('id', ParseIntPipe) id: number, @Body() PortalDto: CreatePortalDto){
        return this.portalService.updatePortal(id, PortalDto);
    }

    @Delete(':id')
    deletePortal(@Param('id', ParseIntPipe) id: number){
       return this.portalService.deletePortal(id);
    }

    @Get(':id')
    getApprovedPortal(@Param('id', ParseIntPipe) id: number){
        return this.portalService.approvedPortal(id);
    }

}
