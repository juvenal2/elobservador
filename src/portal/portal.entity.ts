import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';

@Entity({name:'portal'})
export class Portal{
    @PrimaryGeneratedColumn()
    id_portal: number;
    @Column()
	id_user: number;
    @Column()
	id_type: number;
    @Column()
	id_news: number;
    @Column()
	contents: string;
    @Column()
	approves: boolean;	
    @Column()
	is_active: boolean;
    @Column()
	dates: Date;
}