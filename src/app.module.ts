import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import {TypeOrmModule} from '@nestjs/typeorm';
import { NewsModule } from './news/news.module';
import { TypeNewsModule } from './type-news/type-news.module';
import { CommentaryModule } from './commentary/commentary.module';
import { PortalModule } from './portal/portal.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type:'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'Punio09',
      database: 'TheObserverDB',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false
    }),
    UsersModule,
    NewsModule,
    TypeNewsModule,
    CommentaryModule,
    PortalModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
