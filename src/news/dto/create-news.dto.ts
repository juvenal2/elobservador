export class CreateNewsDto{
    id_news: number;
	id_user: number;
	id_type: number;
	title: string;
	place: string;
	author: string;
	dates: Date;
}