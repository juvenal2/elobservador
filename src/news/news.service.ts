import { Injectable, HttpException, HttpStatus, Body } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {News} from './news.entity'
import { CreateNewsDto } from './dto/create-news.dto';
import { UsersService } from 'src/users/users.service';
import { TypeNewsService } from 'src/type-news/type-news.service';

@Injectable()
export class NewsService {
    constructor(@InjectRepository(News) private newsRepository:Repository<News>,
    private usersService: UsersService,
    private typeNewsService: TypeNewsService
){}

    async createNews(news: CreateNewsDto){
        const userFound = await this.usersService.getUser(news.id_user);
        const typeNewsFound= await this.typeNewsService.getTypeNews(news.id_type);

        if(!userFound) 
            return new HttpException('Usuario no encontrado', HttpStatus.NOT_FOUND);

        if(!typeNewsFound) 
            return new HttpException('Clasificación de noticia no encontrado', HttpStatus.NOT_FOUND);

        news.dates = new Date;

        const newNews = this.newsRepository.create(news);
        return this.newsRepository.save(newNews);
    }

    getNews(){
        return this.newsRepository.find({
            //relations:['user', 'typeNews'],
        });
    }
}
