
import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn} from 'typeorm';
import { User } from 'src/users/user.entity';
import { Commentary } from 'src/commentary/commentary.entity';
import { TypeNews } from 'src/type-news/type-news.entity';


@Entity({name:'news'})
export class News{
    @PrimaryGeneratedColumn()
    id_news: number;

    //@Column({name: 'id_user'})
    @Column()
	id_user: number;

    @Column()
	id_type: number;
    
    @Column()
	title: string;
    @Column()
	place: string;
    @Column()
	author: string;

    @Column({ type: 'date', default: () => 'CURRENT_DATE' })
	dates: Date;

    @ManyToOne(() => User, (user) => user.news)
    @JoinColumn({ name: 'id_user'})
    user:User;

    @ManyToOne(() => TypeNews, (typeNews) => typeNews.news)
    @JoinColumn({ name: 'id_type'})
    typeNews:TypeNews;

    @OneToMany(() => Commentary, (commentary) => commentary.news)
    commentary: Commentary[];
}


