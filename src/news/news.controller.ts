import {Body, Controller, Get, Post} from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
    constructor(private newsService: NewsService){}

    @Post()
    createNews(@Body() news: CreateNewsDto){
        return this.newsService.createNews(news);
    }
    
    @Get()
    getNews(){
        return this.newsService.getNews();
    }
}
