import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { News } from './news.entity';
import { UsersModule } from 'src/users/users.module';
import { Commentary } from 'src/commentary/commentary.entity';
import { TypeNewsModule } from 'src/type-news/type-news.module';

@Module({
  //imports:[ TypeOrmModule.forFeature([News]), UsersModule,
  imports:[ TypeOrmModule.forFeature([News, Commentary]), UsersModule, TypeNewsModule,
  ],
  controllers: [NewsController],
  providers: [NewsService],
  exports: [NewsService]
})
export class NewsModule {}
